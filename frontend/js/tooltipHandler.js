$(document).ready(function () {
    fetchJsonFromUrl();
});

//het ophalen van json data van een url
function fetchJsonFromUrl() {
    var tips = [];

    $.get('https://incentro-apps-overlay.appspot.com/_ah/api/overlay/v1/tooltipcollection', function(response) {

        $.each(response.items, function (key, val) {
            tips.push(val);
            console.log(key, val);
        });

        checkCookie(tips);
    });
}

//kijkt of er een cookie bestaat met daarin de gegevens over de laatst bekeken tip
function checkCookie(tips) {

    var foundCookie = $.cookie('seenTips');

    //als er nog geen cookie bestaat laat de tip zien
    if (isEmpty(foundCookie)) {
        //maak voor iedere tip een nieuwe tip
        for (var i = 0; i < tips.length; i++) {
            //checkt of de tip bij deze pagina hoort, active is en of het element op de pagina gevonden kan worden
            checkTip(tips[i]);
        }
    }
    //check wat er in die cookie staat
    else {
        foundCookie = stringToArray(foundCookie, ",");

        //maak voor iedere tip een nieuwe tip
        /* for (var j = 0; j < tips.length; j++) {
            for (var k = 0; k < foundCookie.length; k++) {
                if (tips[j].tipID != foundCookie[k]) {
                    checkTip(tips[j]);
                }
            }
        }*/

    }

}

//checkt of de tip bij deze pagina hoort, active is en of het element op de pagina gevonden kan worden
function checkTip(tip) {

    // Check of de tip wel active is of niet
    if (tip.active == 1) {

        // Convert id= or class= to # or .
        var convertedElement = convertAttributeToCharacter(tip.htmlElement);
        console.log(convertedElement);
        // Collect element with jQuery
        var arrayOfFoundElementsWithThisClass = $(convertedElement);
        console.log(arrayOfFoundElementsWithThisClass.length);

        // Check if the unique tip can be found
        if (arrayOfFoundElementsWithThisClass.length == 1) {
            makeTip(tip, arrayOfFoundElementsWithThisClass);
        }
    }
}

// Converts id or class to their respective character
function convertAttributeToCharacter(htmlElement) {
    var element;

    if (htmlElement.indexOf("i") == 0) {
        element = htmlElement.replace("id=", "#");
    } else {
        element = htmlElement.replace("class=", ".");
    }

    return element;
}


//een functie die alle andere functies uitvoert om de tip mee aan te maken en het id van de tip die nu getoond word in de cookie op te slaan
function makeTip(tip, targetElement) {

    //check of er al een overlay is, zo niet maak hem aan, anders gebruik de bestaande
    if (!$('body').find('#plug-in-overlay').length) {
        //  console.log("$('body').find('#plug-in-overlay') == null");
        showOverlay();
    }

    addTipToOverlay(tip, targetElement);
}


//maakt enkel de overlay waarop de knoppen / tips komen te staan
function showOverlay() {
    //  console.log("showOverlay");
    //voegt de overlay div toe aan de body van de pagina (als 1e element binnen de body)
    $('body').prepend('<div id="plug-in-overlay"></div>');
}

//voegt de div met tekst toe aan de overlay
function addTipToOverlay(tip, targetElement) {

    if (targetElement.length == 0) return;
    // Identify the tip
    var tipAnchor = '#' + tip.tipID;
    var tipAlignment = tip.align;

    $('#plug-in-overlay').prepend('<div id=' + tip.tipID + ' class="tip tip-' + tipAlignment + '"><span class="arrow arrow-blue arrow-' + tipAlignment + '"></span><span class="arrow arrow-white arrow-' + tipAlignment + '"></span></div>');

    if (tip.title != '') {
        $(tipAnchor).append('<h1>' + tip.title + '</h1>');
    }

    if (tip.description != '') {
        $(tipAnchor).append('<p>' + tip.description + '</p>');
    }

    if (tip.meerInformatie != '') {
        $(tipAnchor).append('<a class="meerInfo" href="' + tip.meerInformatie + '">Meer informatie</a><br />');
    }

    $(tipAnchor).append('<input class="okayButton" type="button" value="Okay"/>');

    positionTip(tip.tipID, tipAlignment, targetElement);

}

// Positioneer de tip adhv de id / class die uit de json komt
function positionTip(tipID, tipAlignment, targetElement) {

    var tip = $("#" + tipID);
    var width = $('body').find(targetElement).width();
    var height = $('body').find(targetElement).height();
    var position = $('body').find(targetElement).offset();

    var x = position.left;
    var y = position.top;

    var newWidth = (width *= 2);

    var tipXOffset;


    // Align tip left
    if (tipAlignment == 'left') {
        tipXOffset = (x -= (newWidth += tip.width()));
    }
    // Align tip right
    else {
        tipXOffset = x += width;
    }

    //alignment top of tip
    y = (y -= (tip.height() / 2));

    $(tip).css("margin-left", tipXOffset + "px");
    $(tip).css("margin-top", y + "px");

    removeTip();
}

// Click handler voor de okay button
function removeTip() {
    $('.okayButton').on("click", function () {
        var tipID = $(this).parent().attr("id");
        $(this).parent().remove();
        checkAndRemoveOverlay();
        makeCookie(tipID);
    });
}

function checkAndRemoveOverlay() {
    if ($('#plug-in-overlay').children().length == 0) {
        $("#plug-in-overlay").remove();
    }
}

//maakt een cookie aan bij de gebruiker om te laten zien welke tip zij als laatst gezien hebben
function makeCookie(tipID) {
 
    var cookieString = $.cookie('seenTips');
    var cookieArray = [];

    if (!isEmpty(cookieString)) {
        cookieArray = stringToArray(cookieString, ",");
    }
    cookieArray.push(tipID);

    $.cookie('seenTips', arrayToString(cookieArray, ","));
}

function emptyCookie(cookie) {
    $.cookie('seenTips', "");
}

function isEmpty(variable) {
    if (variable === undefined || variable === '' || variable === null || variable === "undefined") {
        return true;
    } else {
        return false;
    }
}

// Convert array to string
function arrayToString(array) {

    var stringFromArray = '';
    // Loop through array
    $.each(array, function(key, value) {
        stringFromArray += value+",";
    });

    var lastIndex = stringFromArray.lastIndexOf(",");
    stringFromArray[lastIndex] = "";

    return stringFromArray;
}


// Convert string to array
function stringToArray(string) {
    return string.split(",");
}
