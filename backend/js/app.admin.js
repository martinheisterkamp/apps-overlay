/**
 * Created by j on 18/03/2015.
 */
var latestTipIdFromJSON = '';
var title = '';
var description = '';
var meerInformatie = '';
var active = '';
var align = '';
var htmlElement = '';
var currentURL = '';
var latestTipIDString = '';
var latestTipIdFromJSON;

$(document).ready(function () {
    showOverlay();
});

function fetchJsonFromUrl() {

    $.getJSON("https://incentro-apps-overlay.appspot.com/_ah/api/overlay/v1/tooltipcollection", function (data) {
        data = data.items;
        if (data == '') {
            latestTipIdFromJSON = 0;
        } else {
            $.each(data, function (key, value) {
                
                var checked = value.active;

                if (checked == true) {
                    checked = "checked";
                }

                $('#rightbar').append('<div class="bestaand-evenement"><p class="BE-titel"><input type="checkbox" title="Actief" class="activeToggler" id="activeToggler" data-id="' + value.tipID + '"' + checked + '/>' + value.title + ' <span class="deleteButton" title="Verwijder deze tip" data-id="' + value.tipID + '">X</span><p class="BE-omschrijving">' + value.description + '</p></div>');
            });
            latestTipIdFromJSON = parseInt(data[0].tipID);
        }

    });

}

//maakt enkel de overlay waarop de knoppen / tips komen te staan
function showOverlay() {

    //check of er al een overlay is, zo niet maak hem aan, anders gebruik de bestaande
    if (!$('body').find('#plug-in-overlay').length) {
        //voegt de overlay div toe aan de body van de pagina (als 1e element binnen de body)
        $('body').prepend('<div id="plug-in-overlay"></div>');
    }

    makeUserInterface();
}

//voegt de div met tekst toe aan de overlay
function makeUserInterface() {

    $('#plug-in-overlay').prepend('<div id="rightbar"><div class="barTitle">Bestaande tips</div></div>');
    $('#plug-in-overlay').prepend('<div id="leftbar"><div class="barTitle">Nieuwsfeed Google</div></div>');

    $('#plug-in-overlay').prepend('<div class="tipmaker"></div>');

    $('.tipmaker').append('<div class="evenement-toevoegen">' +
        '<label for="title">Titel van de tip</label>' +
        '<input placeholder="Voer een titel in" required="required" type="text" name="title" id="title"/>' +
        '<label for="description">De uitleg van de tip</label>' +
        '<textarea placeholder="Uitleg van de tip" name="description" required="required" id="description" cols="60" rows="10"></textarea>' +
        '<label for="meerInformatie">Link(optioneel)&colon;</label>' +
        '<input placeholder="Optioneel: Link naar extra informatie" type="text" name="meerInformatie" id="meerInformatie"/>' +
        '<div id="htmlElement"></div>' +
        '<input id="kies-element" type="button" value="Kies Element"/>' +

        '<div class="formButtons">' +
        '<p>Uitlijning van de tip</p>' +
        '<label class="radiolabel" for="posLeft">Links</label>' +
        '<input type="radio" name="align" value="left" id="posLeft" checked="checked" class="radiostyle">' +
        '<label class="radiolabel" for="posRight">Rechts</label>' +
        '<input type="radio" name="align" value="right" id="posRight" class="radiostyle">' +

        '</div>' +

        '<input id="reset" type="button" value="Reset"/>' +
        '<input id="opslaan" type="button" value="Opslaan"/></div>' +
        '<label for="active">Actief</label>' +
        '<input type="checkbox" id="active" name="active">'
    );

    //vul de rightbar met de bestaande tips
    fetchJsonFromUrl();

    //maak de handlers voor alle buttons in het form
    makeButtonHandlers();

    //voeg de rss feed toe van de google updates
    addGoogleRSS();
}


//voegt rss feed toe van Google updates
function addGoogleRSS() {

    //http://googleappsupdates.blogspot.nl/atom.xml
    //https://feeds.feedburner.com/GoogleAppsUpdates?format=xml

    $.ajax({
        url: "//feeds.feedburner.com/GoogleAppsUpdates?format=xml",
        type: "GET",
        dataType: "xml",
        success: function (data) {
            var xml = (new XMLSerializer().serializeToString(data)),
                xmlDoc = $.parseXML(xml),
                $xml = $(xmlDoc);

            $xml.find("entry").each(function () {
                $entry = $(this);
                $title = $entry.find("title");
                $content = $entry.find("content");
                $updated = $entry.find("updated");
                $('#leftbar').append('<div class="rssItem"><h2 class="RSS-titel">' + $title.text() + '</h2><div class="RSS-omschrijving">' + $content.text() + '</div></div>');
            });
            accordion();
        }
    });

}

//zorgt ervoor dat de rss feed items uitklapbaar zijn
function accordion() {

    $('.rssItem').each(function () {
        var $item = $(this);
        $item.on('click', 'h2', function () {
            $item.find('.RSS-omschrijving').toggleClass('RSS-omschrijving-a');
        });
    });
}

//maak handlers voor alle buttons in het formulier
function makeButtonHandlers() {

    save();

    //kies element
    $('#kies-element').on('click', function () {
        //hide de overlay
        $('#plug-in-overlay').hide();

        pickHtmlElement();
    });

    //when clicked, reset everything
    $('#reset').on('click', function () {
        resetFields();
    });

    //make the tip active or inactive
    $('#rightbar').on('change', 'input[type=checkbox]', function () {
        var id = $(this).attr("data-id");
        $.post("https://incentro-apps-overlay.appspot.com/_ah/api/overlay/v1/toggle?id=" + id, function (data) {});
    });

    //delete the tip
    $('#rightbar').on('click', '.deleteButton', function () {
        var id = $(this).attr("data-id");
        var self = $(this);
        $.post("https://incentro-apps-overlay.appspot.com/_ah/api/overlay/v1/deleteObject?id=" + id, function (data) {
            self.closest('.bestaand-evenement').remove();
        });
    });
}

//function to get the properties of a button to add to the tip.
function pickHtmlElement() {

    //loop trough all [button]
    $("[role='button']").each(function () {

        //get properties of element
        var id = $(this).attr('id');


        var offset = $(this).offset();
        var width = $(this).width();
        var height = $(this).height();

        if (width !== '0px' && height !== '0px' && id != '' && id != 'undefined' && id != null) {

            var overlayedButton = $('<div class="overlayButton" id="' + id + '" />');
            overlayedButton.css({
                border: '1px solid blue',
                opacity: '0.6',
                top: offset.top,
                left: offset.left,
                width: width,
                height: height,
                position: 'absolute'

            });

            //make new div in body with ofset and id and color as overlay
            $('body').append(overlayedButton);

        }

    });

    //run function when overlay buttons gets clicked
    $('.overlayButton').each(function () {
        $(this).on('click', function () {

            var htmlElement = '';

            //if there is an id use that
            if ($(this).attr('id') != 'undefined' && $(this).attr('id') != '' && $(this).attr('id') != null) {

                var id = $(this).attr('id');
                htmlElement = '#' + id;
            }


            afterElementIsChosen(htmlElement);

        });
    });

}

//shows the overlay after its been hidden, with additional buttons / options
function afterElementIsChosen(htmlElement) {

    //kies-element knop  niet meer laten zien
    $('#kies-element').hide();

    //click handler voor opnieuw
    $('#reset').show();

    //show overlay again and add htmlElement to div
    $('#plug-in-overlay').show();
    $('#htmlElement').replaceWith('<div id="htmlElement"><b>Gekozen element&colon;</b> <i>' + htmlElement + '</i></div>');
}


//get current page url and call function to pick an element
function getCurrentPageUrl() {
    var currentpageUrl = window.location.href;
    currentpageUrl = currentpageUrl.split('#');
    var currentBaseUrl = currentpageUrl[0];
    return currentBaseUrl;
}

//save function
function save() {

    $('#opslaan').on('click', function () {

        var validationResult = validateUserInput();

        //als de user input valid is
        if (validationResult == "valid") {

            //create the values for the object
            var obj = {
                "title": title,
                "description": description,
                "meerInformatie": meerInformatie,
                "htmlElement": formatElement(htmlElement),
                "currentpageUrl": currentURL,
                "active": active,
                "align": align
            };

            //post the object
            $.post("https://incentro-apps-overlay.appspot.com/_ah/api/overlay/v1/addObject", obj, function (data) {

                //check if success
                //if (data == 'Objects were written to file.') {
                    resetFields();

                    //delete current input of div
                     $('#rightbar').find('.bestaand-evenement').remove();

                    fetchJsonFromUrl();
                //}

            });
        } else {
            alert(validationResult);
        }

    });
}

//htmlobject heeft een selector nu # / . maar omdat # niet in het jsonobject kan parsen we dit naar id={id} of class={class}
function formatElement(myHtmlElement) {

    var JSONformattedHtmlElement = '';
    var splitArray;


    if (myHtmlElement.toLowerCase().indexOf("#") >= 0) {
        splitArray = myHtmlElement.split("#");
        JSONformattedHtmlElement = "id=" + splitArray[1];
    }


    return JSONformattedHtmlElement;
}

//checks whether the uer input misses anything or not
function validateUserInput() {

    //get properties of tip
    title = $('#title').val();
    description = $('#description').val();
    meerInformatie = $('#meerInformatie').val();
    active = $('#active').is(':checked');
    align = $("input[name=align]:checked").val();
    htmlElement = $('#htmlElement').find('i').html();
    currentURL = getCurrentPageUrl();

    // get latestID and add one to itself
    console.log(typeof(latestTipIdFromJSON));
    latestTipIdFromJSON++;

    // Als alle benodigde velden (alles BEHALVE MEER INFORMATIE) ingevuld zijn
    if (isEmpty(title)) {
        return "De titel is niet ingevuld.";
    }
    if (isEmpty(description)) {
        return "De omschrijving is niet ingevuld.";
    }
    if (isEmpty(htmlElement)) {
        return "De class van het element kan niet gevonden worden.";
    }

    return "valid";
}

//reset all fields in forms
function resetFields() {

    $('#title').val('');
    $('#description').val('');
    $('#meerInformatie').val('');
    $('#htmlElement').html('');
    $('#kies-element').show();
    $('#reset').hide();

    //reset var values
    title = '';
    description = '';
    meerInformatie = '';
    active = '';
    align = '';
    htmlElement = '';
    currentURL = '';
    latestTipIDString = '';
}

function isEmpty(object) {
    if (object == '') {
        return true;
    } else {
        return false;
    }
}
