var currentpageUrl, htmlElement;
var tipElements = [];
var evenementPaginaKiezenButton = $('#pagina-kiezen');
var latestID = 0;
var myClassPath= "";
var className = "";

$(document).ready(function(){
    startApp();
});

function startApp() {
    $(document).keyup(function(e) {
        if (e.keyCode == 27) { // escape key
            window.location.reload();        
        }
    });
    
    fetchJsonFromUrl();
    getCurrentPageUrl();
    //showOverlay();
    //createTipForm();
    checkClickedExistingEvent();
    addGoogleRSS();

    //put reset button on hide, show only when element has been chosen
    $('#reset').hide();

    //Save everything
    $('#opslaan').on('click', function () {

        //get properties of tip
        var title = $('#title').val();
        var description = $('#description').val();
        var meerInformatie = $('#meerInformatie').val();
        var active = $('#active').is(':checked');
        var align = $("input[name=align]:checked").val();
        var parentClass = myClassPath;
        var currentURL = currentpageUrl;


        //get current time
        var date = makeDate();

        //make empy object
        var object = {};

        //create key of date now
        var key = date.toString();

        //get latestID and add one to it
        latestID = parseInt(latestID);
        latestID += 1;

        //create the values for the object
        var val = {
            "tipID": "" + latestID,
            "title": "" + title,
            "description": "" + description,
            "meerInformatie": "" + meerInformatie,
            "htmlElement": "" + parentClass,

            "currentpageUrl": "" + currentURL,
            "active": "" + +active,
            "align": "" + align
            };

        //make object
        object[key] = val;

        //stringify the object
        var newTip = JSON.stringify(object);

        //post the object
        $.post("https://stormy-headland-4331.herokuapp.com/index.php?auth=incentroAdmin&action=add&object=" + newTip, function(data) {
            console.log(data);
            console.log(newTip);
            resetFields();

        });

    });

    //when clicked, reset everything
    $('#reset').on('click', function () {
        resetFields();
    });

    //choose an element
    $('#kies-element').on('click', function () {
        $('#plug-in-overlay').hide();
        pickHtmlElement();
    });

    //make the tip active or inactive
    $('#rightbar').on('change', 'input[type=checkbox]', function() {
        var id = $(this).attr("data-id");
        $.post("https://stormy-headland-4331.herokuapp.com/index.php?auth=incentroAdmin&action=toggleObjectActivity&id="+id, function(data) {
        });
    });

    //delete the tip
    $('#rightbar').on('click', '.deleteButton', function() {
        var id = $(this).attr("data-id");
        $.post("https://stormy-headland-4331.herokuapp.com/index.php?auth=incentroAdmin&action=delete&id=" + id, function(data) {
        });
        window.location.reload();

    });
};

//get data from API
function fetchJsonFromUrl() {
    showOverlay();
    $.getJSON("https://stormy-headland-4331.herokuapp.com/?auth=incentro&action=getAllObjects", function (data) {
        $.each(data, function (key, value) {
            var checked = value.active;
            if(checked == "1" || checked == "true"){
                checked = "checked";
            }
            $('#rightbar').append('<div class="bestaand-evenement"><p class="BE-titel"><input type="checkbox" title="Actief" class="activeToggler" id="activeToggler" data-id="'+value.tipID+'"' + checked +'/>' + value.title + ' <span class="deleteButton" title="Verwijder deze tip" data-id="'+value.tipID+'">X</span><p class="BE-omschrijving">' + value.description + '</p></div>');

            latestID = data[0].tipID;
        });
    });


}

//maakt enkel de overlay waarop de knoppen / tips komen te staan
function showOverlay() {
    //voegt de overlay div toe aan de body van de pagina (als 1e element binnen de body)
    $('body').prepend('<div id="plug-in-overlay"></div>');

    createTipForm();
    addExistingTips();
}

//voegt de div met tekst toe aan de overlay
function createTipForm() {
    $('#plug-in-overlay').prepend('<div class="tipmaker"></div>');
    //  $('.tipmaker').append('<div class="evenement-toevoegen"><label for="title">Titel van de tip</label><input placeholder="Voer een titel in" required="required" type="text" name="title" id="title"/><label for="description">De uitleg van de tip</label><textarea placeholder="Uitleg van de tip" name="description" required="required" id="description" cols="60" rows="10"></textarea><label for="meerInformatie">Link (optioneel)</label><input placeholder="Optioneel: Link naar extra informatie" type="text" name="meerInformatie" id="meerInformatie"/><div id="htmlElement"></div><input id="kies-element" type="button" value="Kies Element"/><input class="radiostyle" type="radio" name="position" value="left" id="posLeft"><label class="radiolabel" for="posLeft">links</label><input class="radiostyle" type="radio" name="position" value="right" id="posRight"><label class="radiolabel" for="posRight">Rechts</label><label for="active">Actief</label><input type="checkbox" id="active" name="active" value="true" checked><input id="reset" type="button" value="Reset"/><input id="opslaan" type="button" value="opslaan"/></div>');


    $('.tipmaker').append('<div class="evenement-toevoegen">'+
            '<label for="title">Titel van de tip</label>'+
            '<input placeholder="Voer een titel in" required="required" type="text" name="title" id="title"/>'+
            '<label for="description">De uitleg van de tip</label>'+
            '<textarea placeholder="Uitleg van de tip" name="description" required="required" id="description" cols="60" rows="10"></textarea>'+
            '<label for="meerInformatie">Link(optioneel):</label>'+
            '<input placeholder="Optioneel: Link naar extra informatie" type="text" name="meerInformatie" id="meerInformatie"/>'+
            '<div id="htmlElement"></div>'+
            '<input id="kies-element" type="button" value="Kies Element"/>'+

            '<div class="formButtons">'+
            '<p>Uitlijning van de tip</p>'+
            '<label class="radiolabel" for="posLeft">Links</label>'+
            '<input type="radio" name="align" value="left" id="posLeft" required class="radiostyle">'+
            '<label class="radiolabel" for="posRight">Rechts</label>'+
            '<input type="radio" name="align" value="right" id="posRight" class="radiostyle">'+

            '</div>'+

            '<input id="reset" type="button" value="Reset"/>'+
            '<input id="opslaan" type="button" value="opslaan"/></div>'+
            '<label for="active">Actief</label>'+
            '<input type="checkbox" id="active" name="active">'


    );
}

//laat op basis van de json file de bestaande tips zien
function addExistingTips() {
    $('#plug-in-overlay').prepend('<div id="rightbar"><div class="barTitle">Bestaande tips</div></div>');

    /*$.each(tipElements, function (key, value) {
     $('#rightbar').append('<div class="bestaand-evenement"><p class="BE-titel">' + value.title + '</p><p class="BE-omschrijving">' + value.description + '</p></div>');
     });*/
}

//voegt rss feed toe van Google updates
function addGoogleRSS() {
    $('#plug-in-overlay').prepend('<div id="leftbar"><div class="barTitle">Nieuwsfeed Google</div></div>');
    //http://googleappsupdates.blogspot.nl/atom.xml
    //https://feeds.feedburner.com/GoogleAppsUpdates?format=xml

    $.ajax({
        url: "//feeds.feedburner.com/GoogleAppsUpdates?format=xml",
        type: "GET",
        dataType: "xml",
        success: function (data) {
            var xml = (new XMLSerializer().serializeToString(data)),
                xmlDoc = $.parseXML(xml),
                $xml = $(xmlDoc);

            $xml.find("entry").each(function () {
                $entry = $(this);
                $title = $entry.find("title");
                $content = $entry.find("content");
                $updated = $entry.find("updated");
                $('#leftbar').append('<div class="rssItem"><h2 class="RSS-titel">' + $title.text() + '</h2><div class="RSS-omschrijving">' + $content.text() + '</div></div>');
            });
            accordion();
        }
    });

}


function accordion() {
    $('.rssItem').each(function () {
        var $item = $(this);
        $item.on('click', 'h2', function () {
            $item.find('.RSS-omschrijving').toggleClass('RSS-omschrijving-a');
        });
    });
}

/*check clicked existing event*/
function checkClickedExistingEvent() {
    $('.BE-titel').on('click', function () {


        var currentclickedtitle = $(this).text();
        //console.log("currentclickedtitle = " + currentclickedtitle);

        //ophalen van de data
        //var fetchedjson = fetchJsonFromUrl(''+currentclickedtitle)

        //use data in form
        //fillFormWithExistingEvent();
    });
}

//om het formulier te vullen met een bestaand element uit de lijst van existing events (ul > li)
/*function fillFormWithExistingEvent() {

    $('#title').val(title);
    $('#description').val(description);
    $('#meerInformatie').val(meerInformatie);

}*/


//get current page url and call function to pick an element
function getCurrentPageUrl() {
    $('#kies-element').on('click', function () {
        currentpageUrl = window.location.href;
        console.log("getCurrentPageUrl = " + currentpageUrl);

        currentpageUrl = currentpageUrl.split('#');
        currentpageUrl = currentpageUrl[0];

        JSON.stringify(currentpageUrl);
    });
}

//function to get the properties of a button to add to the tip.
function pickHtmlElement() {

    //loop trough all [button]
    $("[role='button']").each(function () {


        /*className = stripFirstClassName(className);

        var directParentsClass = $(this).parent().attr('class');
        var directParentsClassString = stripFirstClassName(directParentsClass);

        var twoLevelsParentClass = $(this).parent().parent().attr('class');
        var twoLevelsParentClassString = stripFirstClassName(twoLevelsParentClass);

        if (twoLevelsParentClassString !== ".undefined") {
            myClassPath = twoLevelsParentClassString;
        }

        if (directParentsClassString !== ".undefined") {
            myClassPath = myClassPath + " > " + directParentsClassString;
        }

        var classNameString = stripFirstClassName(className);
        if (classNameString !== ".undefined") {
            myClassPath = myClassPath + " > " + classNameString;
        }

        //   console.log("--------------------------------------------------------------------------------");


        myClassPath = myClassPath.trim();

        if(myClassPath[0] == ">")
        {

            myClassPath = myClassPath.substring(2);
        }

        myClassPath = myClassPath.replace("..", " .");

        //    console.log("--------------------------------------------------------------------------------");
*/
        //get properties of element
        var id = $(this).attr('id');
        var className = $(this).attr('class');
        var offset = $(this).offset();
        var width = $(this).width();
        var height = $(this).height();

        //check if width and height are not null/////////////////////////////////////////////////////////////////////////////////////possible fail !==
        if (width !== '0px' && height !== '0px') {

            var overlayedButton = $('<div class="overlayButton" id="' + id + '" />');
            overlayedButton.css({
                border: '1px solid blue',
                opacity: '0.6',
                top: offset.top,
                left: offset.left,
                width: width,
                height: height,
                position: 'absolute'

            });

            //make new div in body with ofset and id and color as overlay
            $('body').append(overlayedButton);
        }
    });

    //run function when overlay buttons gets clicked
    $('.overlayButton').each(function () {
        $(this).on('click', function () {
            console.log("--------------------------------------------------------------------------------");

            console.log("classname = " + className);

           // console.log("myClassPath before = " + myClassPath);


            /*var listItem = "jfk-button-disabled";
            console.log( "Index: " + $( "div" ).index( listItem ) );
            console.log("--------------------------------------------------------------------------------");

            //this is the id that should be added in the db for the tip to be shwon next to
            console.log("myClassPath after = "+$(myClassPath).index());
            //get id  of clicked element*/
            htmlElement = $(this).attr('id');

            //kies-element knop  niet meer laten zien
            $('#kies-element').hide();

            //click handler voor opnieuw
            $('#reset').show();
            //show overlay again and add htmlElement to div
            $('#plug-in-overlay').show();
            $('div#htmlElement').replaceWith('<div id="htmlElement"><b>Gekozen element:</b> <i>' + myClassPath + '</i></div>');
        });
    });

}
//cuts the string on a split

function stripFirstClassName(classNames) {
    //console.log("classNames = "+classNames);

    var myClassNames = String(classNames);
    //console.log("myClassNames string = "+myClassNames);
    var arr = myClassNames.split(" ").join('.');

    //console.log("arr = "+arr);
    return '.' + arr;
}


//reset all fields in forms
function resetFields() {
    $('#title').val('');
    $('#description').val('');
    $('#meerInformatie').val('');
    $('#htmlElement').html('');
    $('#kies-element').show();
    $('#reset').hide()
    currentpageUrl = '';
    myClassPath = "";
    console.log("reset myclasspath = " + myClassPath);
}//give current time and return it.
function makeDate() {
    var myDate = new Date($.now());

    var date = myDate.getFullYear() + '-' + (myDate.getMonth() + 1) + '-' + myDate.getDate();

    var hours = myDate.getHours();
    if (hours < 10) {
        hours = '0' + hours;
    }

    var minutes = myDate.getMinutes();
    if (minutes < 10) {
        minutes = '0' + minutes;
    }

    var seconds = myDate.getSeconds();
    if (seconds < 10) {
        seconds = '0' + seconds;
    }

    var date = date + ' ' + hours + ':' + minutes + ':' + seconds;

    return date;

}