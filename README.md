# YPA Case 2015 - Google Apps overlay #

Here you can find what our product is and how to implement our product.

### Google Apps overlay ###
By creating tooltips and placing them over Google Apps we can help the user understand Google Apps.

### Plug-in installation ###

* Enable Google Chrome extension - developer mode
* Add the folder to your chrome extensions 
* Go to drive or docs and click on the Incentro logo on the top right

### Contact me ###
* Martin Heisterkamp
* martin.heisterkamp@incentro.com
