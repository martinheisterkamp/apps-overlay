package com.incentro;

/**
 * Contains the client IDs and scopes for allowed clients consuming your API.
 */
public class Constants {
  public static final String WEB_CLIENT_ID = "replace this with your web client ID";
  public static final String EMAIL_SCOPE = "https://www.googleapis.com/auth/userinfo.email";
}
