package com.incentro;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;

/**
 * @author Zeger Hoogeboom
 */
@Entity
public class Tooltip
{
	@Id
	Long tipID;
	String title;
	String description;
	String meerInformatie;
	String htmlElement;
	String currentpageUrl;
	String active;
	String align;

	public Tooltip()
	{
	}

	public Tooltip(String title, String description, String meerInformatie, String htmlElement, String currentpageUrl, String active, String align)
	{
		this.title = title;
		this.description = description;
		this.meerInformatie = meerInformatie;
		this.htmlElement = htmlElement;
		this.currentpageUrl = currentpageUrl;
		this.active = active;
		this.align = align;
	}

	public Long getTipID()
	{
		return tipID;
	}

	public void setTipID(Long tipID)
	{
		this.tipID = tipID;
	}

	public String getTitle()
	{
		return title;
	}

	public void setTitle(String title)
	{
		this.title = title;
	}

	public String getDescription()
	{
		return description;
	}

	public void setDescription(String description)
	{
		this.description = description;
	}

	public String getMeerInformatie()
	{
		return meerInformatie;
	}

	public void setMeerInformatie(String meerInformatie)
	{
		this.meerInformatie = meerInformatie;
	}

	public String getHtmlElement()
	{
		return htmlElement;
	}

	public void setHtmlElement(String htmlElement)
	{
		this.htmlElement = htmlElement;
	}

	public String getCurrentpageUrl()
	{
		return currentpageUrl;
	}

	public void setCurrentpageUrl(String currentpageUrl)
	{
		this.currentpageUrl = currentpageUrl;
	}

	public String getActive()
	{
		return active;
	}

	public void setActive(String active)
	{
		this.active = active;
	}

	public String getAlign()
	{
		return align;
	}

	public void setAlign(String align)
	{
		this.align = align;
	}
}
