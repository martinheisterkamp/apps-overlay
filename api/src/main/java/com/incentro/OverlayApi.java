package com.incentro;

import com.google.api.server.spi.config.Api;
import com.google.api.server.spi.config.ApiMethod;
import com.google.api.server.spi.config.Named;
import com.googlecode.objectify.Key;

import java.util.List;
@Api(	name = "overlay",
		version = "v1",
		description = "Endpoints for the columbus intranet project",
		scopes = {Constants.WEB_CLIENT_ID})
public class OverlayApi
{

	@ApiMethod(name="addObject", path = "addObject", httpMethod = "post")
	public void addObject(
			@Named("title") String title,
			@Named("description") String description,
			@Named("meerInformatie") String meerInformatie,
			@Named("htmlElement") String htmlElement,
			@Named("currentpageUrl") String currentpageUrl,
			@Named("active") String active,
			@Named("align") String align)
	{
		OfyService.ofy().save().entity(new Tooltip(title, description, meerInformatie, htmlElement, currentpageUrl, active, align));
	}

	@ApiMethod(name="deleteObject", path = "deleteObject", httpMethod = "post")
	public void deleteObject(@Named("id") String id) throws java.lang.NumberFormatException
	{
		long l = Long.parseLong(id);
		OfyService.ofy().delete().key(Key.create(Tooltip.class, l)).now();
	}

	@ApiMethod(name="getAllObjects", httpMethod = "get")
	public List<Tooltip> getAllObjects()
	{
		return OfyService.ofy().load().type(Tooltip.class).list();
	}

	@ApiMethod(name="toggle", path = "toggle", httpMethod = "post")
	public void toggle(@Named("id") String id)
	{
		Tooltip tooltip = OfyService.ofy().load().type(Tooltip.class).id(Long.parseLong(id)).now();
		if (tooltip != null) {
			if (tooltip.getActive().equals("1")) tooltip.setActive("0");
			else if (tooltip.getActive().equals("0")) tooltip.setActive("1");
			else tooltip.setActive("0");
			OfyService.ofy().save().entity(tooltip);
		}
	}
}
