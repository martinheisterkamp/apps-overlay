package com.incentro;

import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.ObjectifyFactory;
import com.googlecode.objectify.ObjectifyService;

/**
 * @author Zeger Hoogeboom
 */
public class OfyService
{
	static {
		factory().register(Tooltip.class);
	}

	public static Objectify ofy() {
		return ObjectifyService.ofy();
	}
	public static ObjectifyFactory factory() {
		return ObjectifyService.factory();
	}
}
